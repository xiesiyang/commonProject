package com.YbZ.common;

import com.YbZ.exception.CommonDaoException;
import java.util.List;

/**
 * Created by zyb on 2016/09/01.
 */
public interface CommonDao<T> {

    T getById(Object id);

    List<T> getAll();

    T add(T entity) throws CommonDaoException;

    int update(T entity) throws CommonDaoException;

    int delete(T entity) throws CommonDaoException;

    int deleteById(Object id);
}

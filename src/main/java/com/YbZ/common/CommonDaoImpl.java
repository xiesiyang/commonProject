package com.YbZ.common;

import com.YbZ.exception.CommonDaoException;
import com.YbZ.utils.SpellUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.ConnectionCallback;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.util.Assert;

import javax.sql.DataSource;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zyb on 2016/09/01.
 */
public class CommonDaoImpl<T> extends NamedParameterJdbcDaoSupport implements CommonDao<T> {

    private Class<T> entityClass;

    private String tableName;

    private List<String> columns = new ArrayList<String>();

    private List<String> primaryKeys = new ArrayList<String>();

    private Map<String, PropertyDescriptor> mappedFields;

    @Autowired
    @Qualifier("dataSource")
    protected void inject(DataSource dataSource) {
        super.setDataSource(dataSource);
        this.getJdbcTemplate().execute(new ConnectionCallback() {

            public Object doInConnection(Connection conn) throws SQLException, DataAccessException {
                DatabaseMetaData metaData = conn.getMetaData();
                ResultSet rs;
                List<String> pkList = new ArrayList<String>();
                List<String> columnList = new ArrayList<String>();
                // 查询所有字段
                rs = metaData.getColumns(null, null, tableName, null);

                while (rs.next()) {
                    String catlog = rs.getString(1);
                    String schema = rs.getString(2);
                    String name = rs.getString(3);
                    String columnName = rs.getString(4); // COLUMN_NAME
                    columnList.add(columnName);
                }
                rs.close();

                // 查询主键信息
                rs = metaData.getPrimaryKeys(null, null, tableName);
                while (rs.next()) {
                    String catlog = rs.getString(1);
                    String schema = rs.getString(2);
                    String name = rs.getString(3);
                    pkList.add(rs.getString(4));
                }
                rs.close();

                for (String col : columnList) {
                    if (pkList.contains(col)) {
                        primaryKeys.add(col);

                    } else {
                        columns.add(col);
                    }
                }
                return null;
            }
        });
    }

    public CommonDaoImpl() throws CommonDaoException {
        Type genType = getClass().getGenericSuperclass();
        Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
        entityClass = (Class) params[0];
        Field[] fields = entityClass.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            String field = fields[i].getName();
            if (field.equalsIgnoreCase("table_name")) {
                try {
                    tableName = (String) fields[i].get(entityClass.newInstance());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    throw new CommonDaoException("当前pojo不存在table_name字段...");
                } catch (InstantiationException e) {
                    e.printStackTrace();
                    throw new CommonDaoException("当前pojo:" + entityClass.getName() + "无法实例化...");
                }
                break;
            }
        }
        PropertyDescriptor[] pds = BeanUtils.getPropertyDescriptors(entityClass);
        mappedFields = new HashMap<String, PropertyDescriptor>();
        for (int i = 0; i < pds.length; i++) {
            PropertyDescriptor pd = pds[i];
            mappedFields.put(pd.getName(), pd);
        }
    }

    public T getById(Object id) {
        Assert.notEmpty(primaryKeys, "主键不能为空!");
        T entity = null;
        String pk = primaryKeys.get(0);
        String sql = "select * from " + tableName + " t where t." + pk + " = :" + pk;
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put(pk, id);
        List<T> list = this.getNamedParameterJdbcTemplate().query(sql, paramMap, BeanPropertyRowMapper.newInstance(entityClass));
        if (list.size() > 0)
            entity = list.get(0);
        return entity;
    }

    public List<T> getAll() {
        List<T> list = null;
        String sql = "select * from " + tableName;
        list = this.getJdbcTemplate().query(sql, BeanPropertyRowMapper.newInstance(entityClass));
        return list;
    }

    public T add(T entity) throws CommonDaoException {
        List<String> totalCol = (List<String>) CollectionUtils.union(primaryKeys, columns);
        String[] cols = totalCol.toArray(new String[totalCol.size()]);
        logger.info("包含的列有：" + StringUtils.join(totalCol, ","));
        String sql = "insert into " + tableName + "(" + StringUtils.join(totalCol, ",") + ") values(:" + StringUtils.join(totalCol, ",:") + ")";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        for (int i = 0; i < cols.length; i++) {
            String col = cols[i];
            Object fieldValue = getFieldValueByColumnName(col, entity);
            paramMap.put(col, fieldValue);
        }
        //添加主键
        String pk = primaryKeys.get(0);
        Object value = getPkValue(entity);
        paramMap.put(pk, value);
        logger.info("insert sql:" + sql);
        this.getNamedParameterJdbcTemplate().update(sql, paramMap);
        return entity;
    }

    private Object getFieldValueByColumnName(String columnName, T entity) throws CommonDaoException {
        PropertyDescriptor pd = mappedFields.get(columnName);
        Object value = null;
        if (pd == null) {
            String fieldName = SpellUtils.toCamelCase(columnName);
            pd = mappedFields.get(fieldName);
            Method getter = pd.getReadMethod();
            try {
                value = getter.invoke(entity);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                throw new CommonDaoException("当前" + entityClass.getName() + "对象不存在" + fieldName + "属性...");
            } catch (InvocationTargetException e) {
                e.printStackTrace();
                throw new CommonDaoException("获取" + entityClass.getName() + "对象," + fieldName + "属性出现异常...");
            }
        }
        return value;
    }

    public int update(T entity) throws CommonDaoException {
        StringBuffer sql = new StringBuffer("update ");
        sql.append(tableName).append(" set ");
        Map<String, Object> paramMap = new HashMap<String, Object>();
        for (int i = 0; i < columns.size(); i++) {
            String col = columns.get(i);
            String fieldName = SpellUtils.toCamelCase(col);
            Method method = mappedFields.get(fieldName).getReadMethod();
            Object values = null;
            try {
                values = method.invoke(entity);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                throw new CommonDaoException("当前" + entityClass.getName() + "对象不存在" + col + "属性...");
            } catch (InvocationTargetException e) {
                e.printStackTrace();
                throw new CommonDaoException("获取" + entityClass.getName() + "对象," + col + "属性出现异常...");
            }
            paramMap.put(col, values);
            sql.append(col).append("=:").append(col).append(",");
        }
        sql = sql.deleteCharAt(sql.length() - 1);
        String pk = primaryKeys.get(0);
        sql.append(" where ").append(pk).append(" = :").append(pk);
        paramMap.put(pk, getPkValue(entity));
        logger.info("update sql = " + sql.toString());
        return this.getNamedParameterJdbcTemplate().update(sql.toString(), paramMap);
    }

    public int delete(T entity) throws CommonDaoException {
        Object pkValue;
        pkValue = getPkValue(entity);
        if (pkValue != null)
            return deleteById(pkValue);
        else
            return 0;
    }

    private Object getPkValue(T entity) throws CommonDaoException {
        String pk = primaryKeys.get(0);
        PropertyDescriptor pd = mappedFields.get(pk);
        Method method = pd.getReadMethod();
        try {
            return method.invoke(entity);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new CommonDaoException("当前" + entityClass.getName() + "对象不存在" + pk + "属性...");
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            throw new CommonDaoException("获取" + entityClass.getName() + "对象," + pk + "属性出现异常...");
        }
    }

    public int deleteById(Object id) {
        String sql = "delete from " + tableName + " where " + primaryKeys.get(0) + " = :id";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put(primaryKeys.get(0), id);
        logger.info("delete sql : " + sql);
        return this.getNamedParameterJdbcTemplate().update(sql, paramMap);
    }
}

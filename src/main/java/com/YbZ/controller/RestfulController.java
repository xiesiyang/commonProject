package com.YbZ.controller;

import com.YbZ.exception.CommonDaoException;
import com.YbZ.pojo.User;
import com.YbZ.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.util.List;

/**
 * Created by zyb on 2016/09/05.
 */
@RestController
@RequestMapping("/rest")
public class RestfulController {

    @Autowired
    UserService service;

    @RequestMapping("/hello")
    public String hello(){
        return "hello";
    }

    @RequestMapping("getAll")
    public List<User> getAll(){
        List<User> list = service.getAll();
        return list;
    }

    @RequestMapping("getById")
    public User getById(@RequestParam String id){
        return service.getById(id);
    }

    @RequestMapping("update")
    public int update(@RequestParam String id){
        User u = service.getById(id);
        u.setAddress("china");
        int result = -1;
        try {
            result = service.update(u);
        } catch (CommonDaoException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping("/delete")
    public int delete(@RequestParam String id){
        int result = -1;
        User u = service.getById(id);
        try {
            result = service.delete(u);
        } catch (CommonDaoException e) {
            e.printStackTrace();
        }
        return result;
    }

    @RequestMapping("/deleteById")
    public int deleteById(@RequestParam String id){
        return service.deleteById(id);
    }

    @RequestMapping("/add")
    public User add(){
        User u = new User();
        u.setAddress("china");
        u.setSex("0");
        u.setUsername("TMAC");
        u.setId(1);
        u.setBirthday(new Date(System.currentTimeMillis()));
        User au = null;
        try {
            au = service.add(u);
        } catch (CommonDaoException e) {
            e.printStackTrace();
        }
        return au;
    }

    /**
     * 此方法关闭失败，不能正常关闭tomcat，后续在想办法
     * @return
     */
    @RequestMapping("/shutdown")
    public int shutdown(){
        try {
            Runtime.getRuntime().exec("cmd /c E:\\IdeaTomcat\\bin\\shutdown.bat"); // 调用外部程序
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 1;
    }
}

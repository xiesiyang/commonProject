package com.YbZ.dao;

import com.YbZ.common.CommonDaoImpl;
import com.YbZ.exception.CommonDaoException;
import com.YbZ.pojo.User;
import org.springframework.stereotype.Repository;

/**
 * Created by zyb on 2016/09/05.
 */
@Repository
public class UserDao extends CommonDaoImpl<User> {
    public UserDao() throws CommonDaoException {
    }
}

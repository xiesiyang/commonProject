package com.YbZ.exception;

/**
 * Created by zyb on 2016/09/05.
 */
public class CommonDaoException extends Exception {

    private static final long serialVersionUID = -654893533794556357L;

    public CommonDaoException(String errorCode){
        super(errorCode);
    }

    public CommonDaoException(String errorCode, Throwable cause){
        super(errorCode, cause);
    }

    public CommonDaoException(String errorCode, String errorDesc){
        super(errorCode + ":" + errorDesc);
    }

    public CommonDaoException(String errorCode, String errorDesc, Throwable cause){
        super(errorCode + ":" + errorDesc, cause);
    }

    public CommonDaoException(Throwable cause){
        super(cause);
    }
}

package com.YbZ.service;

import com.YbZ.dao.UserDao;
import com.YbZ.exception.CommonDaoException;
import com.YbZ.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zyb on 2016/09/05.
 */
@Service
public class UserService {

    @Autowired
    UserDao userDao;

    public List<User> getAll(){
        return userDao.getAll();
    }

    public User getById(Object id){
        return userDao.getById(id);
    }

    public User add(User u) throws CommonDaoException {
        return userDao.add(u);
    }

    public int update(User u) throws CommonDaoException {
        return userDao.update(u);
    }

    public int delete(User u) throws CommonDaoException {
        return userDao.delete(u);
    }

    public int deleteById(Object id){
        return userDao.deleteById(id);
    }
}

<%--
  Created by IntelliJ IDEA.
  User: Zhang
  Date: 2016/09/05
  Time: 11:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>hello</title>
    <script src="//cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript">
        $.getJSON('rest/hello',function(dat){
            $('#hello').text(dat);
        });
    </script>
</head>
<body>
    <h1>common</h1>
    <div id="hello"></div>
</body>
</html>
